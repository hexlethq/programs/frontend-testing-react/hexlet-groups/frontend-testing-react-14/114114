// . A property is a statement like:
// for all (x, y, ...)
// such that precondition(x, y, ...)
// holds property(x, y, ...)
// is true.

const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('Test sort function properties', () => {
  test('order is ascending', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sortedData = sort(data);

        expect(sortedData).toBeSorted({ ascending: true });
      }),
    );
  });
  // нагуглил что можно проверить а не изменились ли у нас элементы массивов,
  // вроде логичная проверка - чтобы ничего вдруг не пропало.
  test('sorted(data) and data contains the same elements', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sortedData = sort(data);

        for (let index = 1; index < sort.length; index += 1) {
          expect(sortedData).toContain(data[index]);
        }
      }),
    );
  });

  test('array sorted twice returns the same result', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sortedData = sort(data);
        const sortedDataTwice = sort(sortedData);

        expect(sortedData).toEqual(sortedDataTwice);
      }),
    );
  });
});
// END
