const faker = require('faker');

// BEGIN
describe('Test faker', () => {
  const transaction = faker.helpers.createTransaction();

  test('Match transaction object', () => {
    expect(transaction).toMatchObject({
      amount: expect.any(String),
      business: expect.any(String),
      date: expect.any(Date),
      name: expect.any(String),
      type: expect.any(String),
    });
  });

  test('Doesnt mach transaction object', () => {
    const transaction2 = faker.helpers.createTransaction();
    expect(transaction).not.toMatchObject(transaction2);
  });
});
// END
