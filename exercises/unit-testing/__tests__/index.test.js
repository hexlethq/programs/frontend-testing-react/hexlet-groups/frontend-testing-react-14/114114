describe('Test Object assign', () => {
  test('New object equals target', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src); // {k: "v", a: "a", b: "b"}

    expect(result).toEqual({ k: 'v', b: 'b', a: 'a' });
  });

  test('New object doesnt have not enumurable property', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src); // {k: "v", a: "a", b: "b"}
    Object.defineProperty(result, 'notEnumProp', {
      value: 'test',
      enumerable: false,
    });

    expect(Object.prototype.propertyIsEnumerable.call(result, 'notEnumProp')).toBeFalsy();
  });

  test('Reassign not writeable key throws error', () => {
    const target = { k: 'v2', a: 'a' };
    Object.defineProperty(target, 'writeMe', {
      value: 'eatMe',
      writable: false,
    });
    const src = { writeMe: 'drinkMe' };
    const result = () => Object.assign(target, src);

    expect(result).toThrow(TypeError);
  });

  test('New object is a reference of target object', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src); // {k: "v", a: "a", b: "b"}

    expect(target).toBe(result);
  });
});
