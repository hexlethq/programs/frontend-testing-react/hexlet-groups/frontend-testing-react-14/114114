const fs = require('fs');
const path = require('path');

// const getFixturePath = (filename) => path.join(__dirname, '..', '__fixtures__', filename);
// const readFile = (filename) => fs.readFile(getFixturePath(filename), 'utf-8');

// BEGIN
const versionMap = {
  patch: 'patch',
  minor: 'minor',
  major: 'major',
};

const incrementVersion = (version, part = 'patch') => {
  const [major, minor, patch] = version.split('.');
  // split array as strings 1, 3, 2

  switch (part) {
    case versionMap.major:
      return `${+major + 1}.0.0`;

    case versionMap.minor:
      return `${+major}.${+minor + 1}.0`;

    case versionMap.patch:
      return `${+major}.${+minor}.${+patch + 1}`;

    default:
      return version;
  }
};

const upVersion = (filePath, part = versionMap.patch) => {
  const joinedPath = path.join(filePath);
  const version = fs.readFileSync(joinedPath, 'utf-8');
  const versionObject = JSON.parse(version);
  const updatedVersionObject = incrementVersion(versionObject.packageVersion, part);
  const updatedJson = JSON.stringify({ packageVersion: updatedVersionObject });
  fs.writeFileSync(joinedPath, updatedJson, 'utf-8');
};
// END

module.exports = { upVersion };

// upVersion('../__fixtures__/package.json');
