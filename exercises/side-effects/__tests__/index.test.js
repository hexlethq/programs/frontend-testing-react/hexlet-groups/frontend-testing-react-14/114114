const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN

const getFixturePath = (filename) => path.join(__dirname, '..', '__fixtures__', filename);
// const readFile = (filepath) => fs.readFileSync(filepath, 'utf-8');
const initialPackageContent = { packageVersion: '1.3.2' };

const fixturePath = getFixturePath('package.json');

afterEach(() => {
  fs.writeFileSync(fixturePath, JSON.stringify(initialPackageContent), 'utf-8');
});

describe('test side effects', () => {
  test('patch', async () => { // 1.3.3
    upVersion(fixturePath);
    const packageContent = fs.readFileSync(fixturePath, 'utf-8');
    expect(JSON.parse(packageContent)).toEqual({ packageVersion: '1.3.3' });
  });
  test('minor', () => { // 1.4.0
    upVersion(fixturePath, 'minor');
    const packageContent = fs.readFileSync(fixturePath, 'utf-8');
    expect(JSON.parse(packageContent)).toEqual({ packageVersion: '1.4.0' });
  });
  test('major', () => { // 2.0.0
    upVersion(fixturePath, 'major');
    const packageContent = fs.readFileSync(fixturePath, 'utf-8');
    expect(JSON.parse(packageContent)).toEqual({ packageVersion: '2.0.0' });
  });
  test('wrong part is changed', () => {
    upVersion(fixturePath);
    const packageContent = fs.readFileSync(fixturePath, 'utf-8');
    expect(JSON.parse(packageContent)).not.toEqual({ packageVersion: '2.0.0' });
  });
});

// END
