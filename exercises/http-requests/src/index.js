const axios = require('axios');

// BEGIN
// Напишите и протестируйте функцию `get()`,
// которая выполняет GET запрос по указанному URL и возвращает тело ответа.
const get = async (url) => {
  const response = await axios.get(url);
  return response.data;
};
// Напишите и протестируйте функцию `post()`,
// которая выполняет POST запрос по указанному URL и принимает тело запроса вторым аргументом.
const post = async (url, data) => {
  const response = await axios.post(url, data);
  return response.data;
};

// END

module.exports = { get, post };
