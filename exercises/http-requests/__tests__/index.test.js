const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

// `adapter` allows custom handling of requests which makes testing easier.
// Return a promise and supply a valid response (see lib/adapters/README.md).
axios.defaults.adapter = require('axios/lib/adapters/http');

// BEGIN
// тестируем функцию которая дб возвращать список юзеров и создавать его
const expectedData = [
//   {
//     firstname: 'Fedor',
//     lastname: 'Sumkin',
//     age: 33,
//   },
  {
    firstname: 'John',
    lastname: 'Doe',
    age: 42,
  },
];

const baseURL = 'https://example.com';

const usersTestApi = nock(baseURL);

describe('Test http requests', () => {
  beforeAll(() => {
    nock.disableNetConnect();
  });

  afterAll(() => {
    nock.enableNetConnect();
  });

  afterEach(() => {
    nock.cleanAll();
  }); // иначе после первого теста будет 500 ошибка

  test('get users returns 500', async () => {
    usersTestApi
      .get('/users')
      .reply(500);
  });

  test('get users match', async () => {
    // The Interceptor
    // Before executing code that will make a network request that we want to test,
    // we have to setup a mocked network request that will be used
    // in place of the network request in your code:
    usersTestApi // подменяем эндпоинт
      .get('/users') // и урл
      .reply(200, expectedData); // и симулируем удачный ответ

    const response = await get('https://example.com/users'); // перехватываем
    expect(response).toEqual(expectedData);
  });

  test('post users match', async () => {
    const user = {
      firstname: 'John',
      lastname: 'Doe',
      age: 42,
    };
    usersTestApi
      .post('/users')
      .reply(201, user); // дб добавиться юзер

    const response = await post('https://example.com/users', expectedData);
    expect(response).toMatchObject(user);
  });

  test('post users error', async () => {
    usersTestApi
      .post('/users')
      .reply(500);
  });
});

// END
