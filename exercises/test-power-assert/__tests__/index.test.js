const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
// const array = [1, [2, [3, [4]], 5]];
// flattenDepth(array, 1);
// => [1, 2, [3, [4]], 5]
// flattenDepth(array, 2);
// => [1, 2, 3, [4], 5]

// Arguments
// array (Array): The array to flatten.
// [depth=1] (number): The maximum recursion depth.
const array = [1, [2, [3, [4]], 5]];

assert.deepEqual(flattenDepth(array), [1, 2, [3, [4]], 5]);

assert.deepEqual(flattenDepth(array, -1), array);

assert.deepEqual(flattenDepth(array, 0), array);

assert.deepEqual(flattenDepth(array, 1), [1, 2, [3, [4]], 5]);

assert.deepEqual(flattenDepth(array, 2), [1, 2, 3, [4], 5]);

assert.deepEqual(flattenDepth(array, 3), [1, 2, 3, 4, 5]);

assert.deepEqual(flattenDepth([]), []);

// assert.deepEqual(flattenDepth(array, '1'), [1, [2, [3, [4]], 5]]).throws();

// END

// ПЕРЕД ТЕМ КАК НАЧАТЬ РЕШАТЬ ЗАДАНИЕ
// УДАЛИТЕ СЛЕДУЮЩУЮ СТРОКУ
// assert.ok(done);
